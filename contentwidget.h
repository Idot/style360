#ifndef CONTENTWIDGET_H
#define CONTENTWIDGET_H

#include <QWidget>

class QSplitter;
class QLabel;
class QPushButton;
class QToolButton;
class QStyleSheet;

class ContentWidget : public QWidget
{
    Q_OBJECT
public:
    explicit ContentWidget(QWidget *parent = 0);
private:
    QSplitter *main_splitter;
    QSplitter *right_splitter;

    QWidget *left_widget;
    QWidget *right_widget;
    QWidget *right_top_widget;
    QWidget *right_center_widget;
    QWidget *right_center_function_widget;
    QWidget *right_bottom_widget;

    //left widget
    QLabel *label;
    QLabel *suggest_label;
    QLabel *system_safe_label;
    QPushButton *power_button;

    //right top
    QPushButton *login_button;
    QLabel *priv_label;
    QLabel *info_label;
    QLabel *privilege_label;
    QPushButton *register_button;
    QPushButton *safe_button;
    QPushButton *tab_button;
    QPushButton *pet_button;
    QPushButton *lottery_button;
    QPushButton *cloud_five_button;
    QPushButton *caipiao_button;

    //right center
    QToolButton *fireproof_button;
    QToolButton *triggerman_button;
    QToolButton *net_shop_button;
    QLabel *line_label_1;
    QLabel *line_label_2;

    //right center function
    QLabel *function_label;
    QPushButton *more_button;
    QToolButton *recovery_button;
    QToolButton *mobile_button;
    QToolButton *game_box_button;
    QToolButton *desktop_button;
    QToolButton *net_repair_button;
    QToolButton *auto_run_button;
    QToolButton *net_speed_button;
    QToolButton *net_pretect_button;
    QToolButton *first_add_button;

    //right bottom
    QLabel *icon_label;
    QLabel *connect_label;
    QLabel *version_label;
    QPushButton *version_button;

private:
    void initWidgts();
    void initLeft();
    void initRight();
    void initRightTop();
    void initRightCenter();
    void initRightCenterFunction();
    void initRightBottom();

    void translateLanguage();

    //widget init funcitons
    QToolButton* initRightCenterFunctionToolbutton(QPixmap icon,QString style);

};

#endif // CONTENTWIDGET_H

#include "contentwidget.h"

#include <QtWidgets>

ContentWidget::ContentWidget(QWidget *parent) :
    QWidget(parent)
{
    QPalette palette;
    palette.setBrush(QPalette::Window,QBrush(Qt::white));
    setPalette(palette);
    setAutoFillBackground(true);

    this->initWidgts();
    this->translateLanguage();


}

void ContentWidget::initWidgts(){
    this->initLeft();
    this->initRight();
    this->initRightTop();
    this->initRightCenter();
    this->initRightCenter();
    this->initRightCenterFunction();
    this->initRightBottom();

    main_splitter = new QSplitter();

    main_splitter->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
    main_splitter->setOrientation(Qt::Horizontal);
    main_splitter->setHandleWidth(1);
    main_splitter->setStyleSheet("QSplitter::handle{background:lightgray;}");

    right_splitter->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
    right_splitter->setOrientation(Qt::Vertical);
    right_splitter->setHandleWidth(1);
    right_splitter->setStyleSheet("QSplitter::handle{background:lightgray}");

    right_top_widget->setFixedSize(250,130);
    right_center_widget->setFixedSize(250,90);
    right_bottom_widget->setFixedSize(250,30);

    right_splitter->addWidget(right_top_widget);
    right_splitter->addWidget(right_center_widget);
    right_splitter->addWidget(right_center_function_widget);
    right_splitter->addWidget(right_bottom_widget);

    main_splitter->addWidget(left_widget);
    main_splitter->addWidget(right_splitter);

    for (int i = 0; i < right_splitter->count(); ++i) {
        QSplitterHandle *handle = right_splitter->handle(i);
        handle->setEnabled(false);
    }

    for (int i = 0; i < main_splitter->count(); ++i) {
        QSplitterHandle *handle = main_splitter->handle(i);
        handle->setEnabled(false);
    }

    QHBoxLayout *main_layout = new QHBoxLayout();
    main_layout->addWidget(main_splitter);
    main_layout->setSpacing(0);
    main_layout->setContentsMargins(0,0,0,0);

    setLayout(main_layout);
}

void ContentWidget::initLeft(){
    left_widget = new QWidget();
    label = new QLabel();
    suggest_label =new QLabel();
    system_safe_label = new QLabel();
    power_button = new QPushButton();

    left_widget->resize(650,500);

    QPixmap label_pixmap("://img/contentWidget/computer.png");
    label->setPixmap(label_pixmap);
    label->setFixedSize(label_pixmap.size());

    QFont font = suggest_label->font();
    font.setPointSize(12);
    font.setBold(true);
    suggest_label->setFont(font);
    suggest_label->setStyleSheet("color:gray;");

    font = system_safe_label->font();
    font.setBold(true);
    system_safe_label->setFont(font);
    system_safe_label->setStyleSheet("color:gray;");

    QPixmap pixmap("://img/contentWidget/power.png");
    power_button->setIcon(pixmap);
    power_button->setIconSize(pixmap.size());
    power_button->setFixedSize(180,70);
    power_button->setStyleSheet("QPushButton{border-radius:5px;background:rgb(100,190,10)};"
                                "QPushButton:hover{background:rgb(140,220,35)};");

    font = power_button->font();
    font.setPointSize(16);
    power_button->setFont(font);

    QVBoxLayout *v_layout =new QVBoxLayout();
    v_layout->addWidget(suggest_label);
    v_layout->addWidget(system_safe_label);
    v_layout->addStretch();
    v_layout->addSpacing(15);
    v_layout->setContentsMargins(0,20,0,0);

    QHBoxLayout *h_layout =new QHBoxLayout();
    h_layout->addWidget(label,0,Qt::AlignCenter);
    h_layout->addLayout(v_layout);
    h_layout->addStretch();
    h_layout->setSpacing(20);
    h_layout->setContentsMargins(30,20,0,0);

    QVBoxLayout *main_layout = new QVBoxLayout();
    main_layout->addLayout(h_layout);
    main_layout->addWidget(power_button,0,Qt::AlignCenter);
    main_layout->addStretch();
    main_layout->addSpacing(0);
    main_layout->setContentsMargins(0,0,0,0);

    left_widget->setLayout(main_layout);
    left_widget->setMinimumWidth(500);
}

void ContentWidget::initRight(){
    right_splitter =new QSplitter();
}

void ContentWidget::initRightTop(){
    right_top_widget = new QWidget();
    login_button = new QPushButton();
    priv_label = new QLabel();
    info_label = new QLabel();
    privilege_label = new QLabel();
    register_button = new QPushButton();
    safe_button = new QPushButton();
    tab_button = new QPushButton();
    pet_button = new QPushButton();
    lottery_button = new QPushButton();
    cloud_five_button = new QPushButton();
    caipiao_button = new QPushButton();

    login_button->setFixedSize(240,60);
    login_button->setStyleSheet("QPushButton{color:green;border-image:url(://img/contentWidget/login.png)};");
    QFont font = login_button->font();
    font.setBold(true);
    font.setPointSize(12);
    login_button->setFont(font);

    priv_label->setPixmap(QPixmap("://img/contentWidget/priv.png"));

    QPixmap safePixmap("://img/contentWidget/360.png");
    safe_button->setIcon(safePixmap);
    safe_button->setIconSize(safePixmap.size());

    QPixmap tabPixmap("://img/contentWidget/tab.png");
    tab_button->setIcon(tabPixmap);
    tab_button->setIconSize(tabPixmap.size());

    QPixmap petPixmap("://img/contentWidget/pet.png");
    pet_button->setIcon(petPixmap);
    pet_button->setIconSize(petPixmap.size());

    QPixmap lotteryPixmap("://img/contentWidget/lottery.png");
    lottery_button->setIcon(lotteryPixmap);
    lottery_button->setIconSize(lotteryPixmap.size());

    QPixmap cloudFivePixmap("://img/contentWidget/cloud_five.png");
    cloud_five_button->setIcon(cloudFivePixmap);
    cloud_five_button->setIconSize(cloudFivePixmap.size());

    QPixmap caipiaoPixmap("://img/contentWidget/cloud_five.png");
    caipiao_button->setIcon(caipiaoPixmap);
    caipiao_button->setIconSize(caipiaoPixmap.size());

    register_button->setCursor(Qt::PointingHandCursor);
    safe_button->setCursor(Qt::PointingHandCursor);
    tab_button->setCursor(Qt::PointingHandCursor);
    pet_button->setCursor(Qt::PointingHandCursor);
    lottery_button->setCursor(Qt::PointingHandCursor);
    cloud_five_button->setCursor(Qt::PointingHandCursor);
    caipiao_button->setCursor(Qt::PointingHandCursor);

    register_button->setStyleSheet("color:rgb(0,120,30);background:transparent;");
    safe_button->setStyleSheet("background:transparent;");
    tab_button->setStyleSheet("background:transparent");
    pet_button->setStyleSheet("background:transparent");
    lottery_button->setStyleSheet("background:transparent");
    cloud_five_button->setStyleSheet("background:transparent");
    caipiao_button->setStyleSheet("background:transparent");

    QHBoxLayout *login_layout=new QHBoxLayout();
    login_layout->addWidget(login_button);
    login_layout->addStretch();
    login_layout->setContentsMargins(15,0,0,0);

    QHBoxLayout *register_layout = new QHBoxLayout();
    register_layout->addStretch();
    register_layout->addWidget(privilege_label);
    register_layout->addWidget(info_label);
    register_layout->addWidget(register_button);
    register_layout->addStretch();
    register_layout->addSpacing(5);
    register_layout->setContentsMargins(0,0,0,0);

    QHBoxLayout *privilege_layout = new QHBoxLayout();
    privilege_layout->addStretch();
    privilege_layout->addWidget(privilege_label);
    privilege_layout->addWidget(safe_button);
    privilege_layout->addWidget(tab_button);
    privilege_layout->addWidget(pet_button);
    privilege_layout->addWidget(lottery_button);
    privilege_layout->addWidget(cloud_five_button);
    privilege_layout->addWidget(caipiao_button);
    privilege_layout->addStretch();
    privilege_layout->addSpacing(8);
    privilege_layout->setContentsMargins(0,0,0,0);

    QVBoxLayout *main_layout =new QVBoxLayout();
    main_layout->addStretch();
    main_layout->addLayout(login_layout);
    main_layout->addLayout(register_layout);
    main_layout->addLayout(privilege_layout);
    main_layout->addStretch();
    main_layout->addSpacing(5);
    main_layout->setContentsMargins(10,10,10,10);

    right_top_widget->setLayout(main_layout);
}

void ContentWidget::initRightCenter(){
    right_center_widget = new QWidget();
    fireproof_button = new QToolButton();
    triggerman_button = new QToolButton();
    net_shop_button = new QToolButton();
    line_label_1 = new QLabel();
    line_label_2 = new QLabel();

    line_label_1->setFixedWidth(10);
    line_label_2->setFixedWidth(10);

    line_label_1->installEventFilter(this);
    line_label_2->installEventFilter(this);

    fireproof_button->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    triggerman_button->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    net_shop_button->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);

    QPixmap fireproofPixmap("://img/contentWidget/fireproof.png");
    fireproof_button->setIcon(fireproofPixmap);
    fireproof_button->setIconSize(fireproofPixmap.size());
    fireproof_button->setFixedSize(fireproofPixmap.width()+25,fireproofPixmap.height()+25);

    QPixmap triggermanPixmap("://img/contentWidget/triggerman.png");
    triggerman_button->setIcon(triggermanPixmap);
    triggerman_button->setIconSize(triggermanPixmap.size());
    triggerman_button->setFixedSize(triggermanPixmap.width()+25,triggermanPixmap.height()+25);

    QPixmap netShopPixmap("://img/contentWidget/net_shop.png");
    net_shop_button->setIcon(netShopPixmap);
    net_shop_button->setIconSize(netShopPixmap.size());
    net_shop_button->setFixedSize(netShopPixmap.width()+25,netShopPixmap.height()+25);

    fireproof_button->setStyleSheet("background:transparent");
    triggerman_button->setStyleSheet("background:transparent");
    net_shop_button->setStyleSheet("background:transparent");

    QHBoxLayout *h_layout = new QHBoxLayout();
    h_layout->addWidget(fireproof_button);
    h_layout->addWidget(line_label_1);
    h_layout->addWidget(triggerman_button);
    h_layout->addWidget(line_label_2);
    h_layout->addWidget(net_shop_button);
    h_layout->setSpacing(0);
    h_layout->setContentsMargins(0,0,0,0);

    right_center_widget->setLayout(h_layout);
}

QToolButton* ContentWidget::initRightCenterFunctionToolbutton(QPixmap icon, QString style)
{
    QToolButton *b = new QToolButton;
    b->setIcon(icon);
    b->setIconSize(icon.size());
    b->setFixedSize(icon.width()+50,icon.height()+50);
    b->setStyleSheet(style);
    return b;
}

void ContentWidget::initRightCenterFunction(){
    right_center_function_widget = new QWidget();
    function_label = new QLabel();
    more_button = new QPushButton();

    QFont font = function_label->font();
    font.setBold(true);
    function_label->setFont(font);
    function_label->setStyleSheet("color:green");

    more_button->setFixedSize(50,25);
    more_button->setStyleSheet("QPushButton:rgb(0,120,230);background:transparent;");
    more_button->setCursor(Qt::PointingHandCursor);

    QHBoxLayout *h_layout = new QHBoxLayout();
    h_layout->addWidget(function_label);
    h_layout->addStretch();
    h_layout->addWidget(more_button);
    h_layout->setSpacing(0);
    h_layout->setContentsMargins(10,5,0,0);

    QString styleSheet("QToolButton{background:transparent}");
    recovery_button=initRightCenterFunctionToolbutton(QPixmap("://img/contentWidget/recovery.png"),
                                                      styleSheet);
    mobile_button=initRightCenterFunctionToolbutton(QPixmap("://img/contentWidget/mobile.png"),
                                                    styleSheet);
    game_box_button=initRightCenterFunctionToolbutton(QPixmap("://img/contentWidget/game_box.png"),
                                                      styleSheet);
    desktop_button=initRightCenterFunctionToolbutton(QPixmap("://img/contentWidget/desktop.png"),
                                                     styleSheet);
    net_repair_button=initRightCenterFunctionToolbutton(QPixmap("://img/contentWidget/net_repair.png"),
                                                        styleSheet);
    auto_run_button=initRightCenterFunctionToolbutton(QPixmap("://img/contentWidget/auto_run.png"),
                                                      styleSheet);
    net_speed_button=initRightCenterFunctionToolbutton(QPixmap("://img/contentWidget/net_speed.png"),
                                                       styleSheet);
    net_pretect_button=initRightCenterFunctionToolbutton(QPixmap("://img/contentWidget/net_pretext.png"),
                                                         styleSheet);
    first_add_button=initRightCenterFunctionToolbutton(QPixmap("://img/contentWidget/first_add.png"),
                                                       styleSheet);
    QGridLayout *grid_layout = new QGridLayout();
    grid_layout->addWidget(recovery_button,0,0);
    grid_layout->addWidget(mobile_button,0,1);
    grid_layout->addWidget(game_box_button,0,2);
    grid_layout->addWidget(desktop_button,1,0);
    grid_layout->addWidget(net_repair_button,1,1);
    grid_layout->addWidget(auto_run_button,1,2);
    grid_layout->addWidget(net_speed_button,3,0);
    grid_layout->addWidget(net_pretect_button,3,1);
    grid_layout->addWidget(first_add_button,3,2);
    grid_layout->setSpacing(0);
    grid_layout->setContentsMargins(5,0,5,5);

    QVBoxLayout *v_layout = new QVBoxLayout();
    v_layout->addLayout(h_layout);
    v_layout->addLayout(grid_layout);
    v_layout->addStretch();
    v_layout->setSpacing(10);
    v_layout->setContentsMargins(0,0,0,0);
    right_center_function_widget->setLayout(v_layout);
}

void ContentWidget::initRightBottom(){
    right_bottom_widget = new QWidget();
    icon_label = new QLabel();
    connect_label = new QLabel();
    version_label = new QLabel();
    version_button = new QPushButton();

    QPixmap labelPixmap("://img/contentWidget/cloud.png");
    icon_label->setPixmap(labelPixmap);
    icon_label->setFixedSize(labelPixmap.size());

    QPixmap pixmap("://img/contentWidget/version.png");
    version_button->setIcon(pixmap);
    version_button->setIconSize(pixmap.size());
    version_button->setFixedSize(20,20);
    version_button->setStyleSheet("background:transparent");

    QHBoxLayout *bottom_layout = new QHBoxLayout();
    bottom_layout->addWidget(icon_label);
    bottom_layout->addWidget(connect_label);
    bottom_layout->addStretch();
    bottom_layout->addWidget(version_label);
    bottom_layout->setSpacing(5);
    bottom_layout->setContentsMargins(10,0,10,0);
    right_bottom_widget->setLayout(bottom_layout);
}

void ContentWidget::translateLanguage(){
    suggest_label->setText(tr("suggest"));
    system_safe_label->setText(tr("system info"));
    power_button->setText(tr("power"));

    login_button->setText(tr("login home"));
    info_label->setText(tr("show beautiful icon"));
    register_button->setText(tr("register"));
    privilege_label->setText(tr("pribilege power"));

    fireproof_button->setText(tr("fireproof"));
    triggerman_button->setText(tr("trigger man"));
    net_shop_button->setText(tr("net shop"));

    function_label->setText(tr("function"));
    more_button->setText(tr("more"));
    recovery_button->setText(tr("recovry"));
    mobile_button->setText(tr("moblie"));
    game_box_button->setText(tr("gamebox"));
    desktop_button->setText(tr("desktop"));
    net_repair_button->setText(tr("net repair"));
    auto_run_button->setText(tr("auto run"));
    net_speed_button->setText(tr("net speed"));
    net_pretect_button->setText(tr("net pretext"));
    first_add_button->setText(tr("first add"));

    connect_label->setText(tr("connext success"));
    version_label->setText(tr("version"));

}

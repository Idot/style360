#-------------------------------------------------
#
# Project created by QtCreator 2013-11-15T14:33:35
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = style360
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    contentwidget.cpp

HEADERS  += mainwindow.h \
    contentwidget.h

RESOURCES += \
    img.qrc

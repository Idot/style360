#include "mainwindow.h"
#include <QtWidgets>


MainWindow::MainWindow(QWidget *parent)
    : QWidget(parent)
{
    setWindowFlags(Qt::FramelessWindowHint);
    QPalette palette;
    palette.setBrush(QPalette::Window,QBrush(Qt::white));
    setPalette(palette);
    setAutoFillBackground(true);

    setFixedSize(650,500);

    init();
}
void MainWindow::init(){
    label = new QLabel("label");
    suggest_label = new QLabel("suggest");
    system_safe_label = new QLabel("system safe");
    power_button = new QPushButton;

    QPixmap label_pixmap("://img/contentWidget/computer.png");
    label->setPixmap(label_pixmap);
    label->setFixedSize(label_pixmap.size());

    QFont suggest_font = suggest_label->font();
    suggest_font.setPointSize(12);
    suggest_font.setBold(true);
    suggest_label->setFont(suggest_font);
    suggest_label->setStyleSheet("color:gray");

    QFont system_safe_font = system_safe_label->font();
    system_safe_font.setBold(true);
    system_safe_label->setFont(system_safe_font);
    system_safe_label->setStyleSheet("color:gray");

    QPixmap pixmap("://img/contentWidget/power.png");
    QIcon icon(pixmap);
    power_button->setIcon(icon);
    power_button->setIconSize(pixmap.size());
    power_button->setFixedSize(180,70);
    power_button->setStyleSheet("QPushButton{border-radius:5px;background:rgb(110,190,10)}");
    QFont power_font = power_button->font();
    power_font.setPointSize(16);
    power_button->setFont(power_font);

    QVBoxLayout *v_layout = new QVBoxLayout();
    v_layout->addWidget(suggest_label);
    v_layout->addWidget(system_safe_label);
    v_layout->addStretch();
    v_layout->setSpacing(15);
    v_layout->setContentsMargins(0,20,0,0);

    QHBoxLayout *h_layout=new QHBoxLayout();
    h_layout->addWidget(label,Qt::AlignTop);
    h_layout->addLayout(v_layout);
    h_layout->addStretch();
    h_layout->addSpacing(20);
    h_layout->setContentsMargins(30,20,0,0);

    QVBoxLayout *main_layout = new QVBoxLayout();
    main_layout->addLayout(h_layout);
    main_layout->addWidget(power_button,0,Qt::AlignCenter);
    main_layout->addStretch();
    main_layout->addSpacing(0);
    main_layout->setContentsMargins(0,0,0,0);

    setLayout(main_layout);
}

MainWindow::~MainWindow()
{

}

void MainWindow::mouseDoubleClickEvent(QMouseEvent *){
    qApp->exit();
}
